package cn.stylefeng.roses.kernel.sync.modular.cc;

import cn.stylefeng.roses.kernel.sync.config.properties.CanalProperties;
import com.alibaba.otter.canal.client.CanalConnector;
import com.alibaba.otter.canal.client.CanalConnectors;
import org.springframework.beans.factory.annotation.Autowired;

import java.net.InetSocketAddress;

/**
 * 单机模式canal客户端
 *
 * @author jianghang 2013-4-15 下午04:19:20
 * @version 1.0.4
 */
public class SimpleCanalClient extends AbstractCanalClient {

    @Autowired
    private CanalProperties canalProperties;

    public SimpleCanalClient(String destination) {
        super(destination);
    }

    public void init() {
        CanalConnector connector = CanalConnectors.newSingleConnector(
                new InetSocketAddress(canalProperties.getAddress(), canalProperties.getPort()), this.destination, "", "");

        this.setConnector(connector);
        this.start();

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            try {
                logger.info("## stop the canal client");
                this.stop();
            } catch (Throwable e) {
                logger.warn("##something goes wrong when stopping canal:", e);
            } finally {
                logger.info("## canal client is down.");
            }
        }));
    }

}