package cn.stylefeng.roses.kernel.dict.modular.controller;

import cn.stylefeng.roses.core.page.PageFactory;
import cn.stylefeng.roses.kernel.dict.modular.entity.DictType;
import cn.stylefeng.roses.kernel.dict.modular.model.DictTypeInfo;
import cn.stylefeng.roses.kernel.dict.modular.service.DictTypeService;
import cn.stylefeng.roses.kernel.model.response.ResponseData;
import cn.stylefeng.roses.kernel.model.response.SuccessResponseData;
import cn.stylefeng.roses.kernel.scanner.modular.annotation.PostResource;
import cn.stylefeng.roses.kernel.scanner.modular.stereotype.ApiResource;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


/**
 * 字典类型管理
 *
 * @author fengshuonan
 * @Date 2018/7/25 下午12:47
 */
@RestController
@ApiResource(name = "字典类型管理", path = "/dictType")
@Api(tags = "字典类型接口")
public class DictTypeController {

    @Autowired
    private DictTypeService dictTypeService;

    /**
     * 获取字典类型列表
     *
     * @author fengshuonan
     * @Date 2018/7/25 下午12:36
     */
    @ApiOperation("获取字典类型列表")
    @PostResource(name = "获取字典类型列表", path = "/getDictTypeList", requiredPermission = false)
    public ResponseData getDictTypeList(@RequestBody DictTypeInfo dictTypeInfo) {
        Page<DictTypeInfo> page = PageFactory.defaultPage();
        List<DictTypeInfo> dictTypeList = dictTypeService.getDictTypeList(page, dictTypeInfo);
        page.setRecords(dictTypeList);
        return new SuccessResponseData(page);
    }

    /**
     * 添加字典类型
     *
     * @author fengshuonan
     * @Date 2018/7/25 下午12:36
     */
    @ApiOperation("添加字典类型")
    @PostResource(name = "添加字典类型", path = "/addDictType", requiredPermission = false)
    public ResponseData addDictType(@RequestBody DictType dictType) {
        this.dictTypeService.addDictType(dictType);
        return new SuccessResponseData();
    }

    /**
     * 修改字典类型
     *
     * @author fengshuonan
     * @Date 2018/7/25 下午12:36
     */
    @ApiOperation("修改字典类型")
    @PostResource(name = "修改字典类型", path = "/updateDictType", requiredPermission = false)
    public ResponseData updateDictType(@RequestBody DictType dictType) {
        this.dictTypeService.updateDictType(dictType);
        return new SuccessResponseData();
    }

    /**
     * 删除字典类型
     *
     * @author fengshuonan
     * @Date 2018/7/25 下午12:36
     */
    @ApiOperation("删除字典类型")
    @PostResource(name = "删除字典类型", path = "/deleteDictType", requiredPermission = false)
    public ResponseData deleteDictType(@RequestParam("dictTypeId") String dictTypeId) {
        this.dictTypeService.deleteDictType(dictTypeId);
        return new SuccessResponseData();
    }

    /**
     * 修改字典类型状态
     *
     * @author fengshuonan
     * @Date 2018/7/25 下午12:36
     */
    @ApiOperation("修改字典类型状态")
    @PostResource(name = "修改字典类型状态", path = "/updateStatus", requiredPermission = false)
    public ResponseData updateStatus(@RequestParam("dictTypeId") String dictTypeId, @RequestParam("dictStatus") Integer dictStatus) {
        this.dictTypeService.updateDictTypeStatus(dictTypeId, dictStatus);
        return new SuccessResponseData();
    }

    /**
     * code校验
     *
     * @return true：可以使用；false：不能使用
     * @author fengshuonan
     * @Date 2018年11月16日
     */
    @ApiOperation("code校验")
    @PostResource(name = "code校验", path = "/checkCode", requiredPermission = false)
    @ApiImplicitParams({@ApiImplicitParam(name = "dictTypeCode", required = true, value = "不能为空"), @ApiImplicitParam(name = "dictTypeId", required = true, value = "可以传空")})
    public ResponseData checkCode(@RequestBody DictType dictType) {
        String dictTypeCode = dictType.getDictTypeCode();
        String dictTypeId = dictType.getDictTypeId();
        boolean flag = this.dictTypeService.checkCode(dictTypeCode, dictTypeId);
        return new SuccessResponseData(flag);
    }

}
